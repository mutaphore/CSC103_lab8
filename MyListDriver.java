import java.util.Scanner;


public class MyListDriver {

   public static void main(String[] args) {
      
      MyList list = new MyList();
      char choice = 'z';
      Scanner input = new Scanner(System.in);
      int temp;
      
      System.out.println("Choose one of the following operations:");
      System.out.println("- add (enter the letter a)");
      System.out.println("- find (enter the letter f)");
      System.out.println("- sum (enter the letter s)");
      System.out.println("- print (enter the letter p");
      System.out.println("- quit (enter the letter q)");
      
      
      
      while(choice != 'q')
      {
         System.out.println("\nEnter choice: ");
         choice = input.nextLine().charAt(0);
         
         switch(choice) {
         
         case 'a':
            System.out.println("Enter an integer to be added: ");
            temp = input.nextInt();
            input.nextLine();
            list.add(temp);
            break;
            
         case 'f':
            System.out.print("Enter integer to find: ");
            temp = input.nextInt();
            input.nextLine();
            if(list.find(temp))
               System.out.println("Found element");
            else
               System.out.println("Did not find element ...");
            break;
            
         case 's':
            System.out.println("The sum is " + list.sum());
            break;
            
         case 'p':
            list.print();
            break;
         
         case 'q':
            System.out.println("Quitting!");
            break;
         
         default:
            System.out.println("Invalid choice!");
            break;
          
         }
         
      }
      
   }
   
}
