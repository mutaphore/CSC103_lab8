
public class MyList {

   private Node head;
   
   private class Node {
     
      public int element;
      Node link;
      
      public Node(int data) {
         element = data;
         link = null;
      }
      
   }
   
   public MyList () {
      
      head = null;
      
   }
   
   public void add(int data) {
      
      Node temp = new Node(data);
      temp.link = head;
      head = temp;
      
   }
   
   public boolean find(int target) {
      
      if(head == null)
         return false;
      
      return find(target, head);
      
   }
   
   private boolean find(int target, Node first) {
      
      if(first.element == target) 
         return true;
      else if(first.link == null)
         return false;
      else
         return find(target,first.link);
      
   }
   
   public void print() {
      
      if(head != null)
         print(head);
      
   }
   
   private void print(Node first) {
      
      if(first.link != null)
         print(first.link);
         
      System.out.print(first.element + " ");
      
   }
   
   public int sum() {
      
      return sum(head);
      
   }
   
   private int sum(Node first) {
            
      if(first.link != null)
         return sum(first.link) + first.element;
      else
         return first.element;
      
   }
   
   
   
   
}
